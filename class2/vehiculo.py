class vehiculo:

    '''esta es la clase vehiculo'''

    def __init__(self,marca,velocidad):
        self.marca=marca
        self.vel=velocidad

        self.___password="fabian" #atributo privado
        self._protegido="ffff"
    
    def velocidad(self):
        doblevel=2*self.vel
        return doblevel

class moto(vehiculo):

    '''docstring'''

    def __init__(self,marca,velocidad,numllantas):
        super().__init__(marca)
        self.numllantas=numllantas

    def velocidad(self):
        doblevel=20*self.vel
        return doblevel

class carro(moto):
    '''clase carro'''
    def __init__(self,marca,velocidad,numllantas,propulsion):
        super().__init__(marca,velocidad,numllantas)
        self.propulsion=propulsion