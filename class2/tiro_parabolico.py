import numpy as np


class Tiroparabolico:

    '''Clase de tiro parabólico'''

    def __init__(self,alpha,v0,h0): #alpha: ángulo de salida; v0: velocidad inicial; h0: altura inicial
        self.alpha=alpha
        self.v0=v0
        self.h0=h0
