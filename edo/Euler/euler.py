import numpy as np
import matplotlib.pyplot as plt


class edo:

    '''EDO
    a= es el print'''

    def __init__(self,a,b,n,y0,f,punto=False):
        self.a=a
        self.b=b
        self.n=n
        self.y0=y0
        self.punto=punto
        self.x=[]
        self.y=[]
        self.f=f

    
    def h(self):
        if self.punto:
            return(self.punto-self.a)/self.n
        else:
            return(self.b-self.a)/self.n

    def X(self):
        if self.punto:
            self.x=np.arange(self.a,self.punto+self.h(),self.h())
        else:
            self.x=np.arange(self.a,self.b+self.h(),self.h())
        
        return self.x
    
    def euler(self):
        self.X()

        self.y=[self.y0]
        for i in range(self.n):
            self.y.append(self.y[i]+self.h()\
                *self.f(self.x[i],self.y[i]))
        
        #poner la condición del punto

        return self.x, self.y
    
    def solanalitica(self):
        return "sol"

    def figplot(self):
        plt.figure(figsize=(10,6))
        xeuler,yeuler=self.euler()
        plt.plot(xeuler,yeuler,label="sol_euler")
        plt.legend()
        plt.savefig("soleuler.png")
